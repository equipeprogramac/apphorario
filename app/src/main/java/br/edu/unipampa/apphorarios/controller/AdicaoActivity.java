package br.edu.unipampa.apphorarios.controller;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.format.DateFormat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TimePicker;

import java.util.ArrayList;
import java.util.List;

import br.edu.unipampa.apphorarios.R;
import br.edu.unipampa.apphorarios.adapter.AdapterFormDia;
import br.edu.unipampa.apphorarios.model.Aula;
import br.edu.unipampa.apphorarios.model.Disciplina;
import br.edu.unipampa.apphorarios.persistencia.DataBasePersistencia;
import br.edu.unipampa.apphorarios.util.TempoUtils;

public class AdicaoActivity extends AppCompatActivity {
    protected RecyclerView recyclerView;
    protected Disciplina disciplina;
    protected static int TEMPO_NORIFICACAO = -1;
    // variáveis que representam os atributos da disciplinas, ou seja, todas as aulas têm em comum
    protected EditText nome;
    protected EditText professor;

    //   private EditText curso;
    protected EditText turma;
    protected EditText semestre;
    protected AutoCompleteTextView curso;

    //variavel da cbNotificacao
    protected CheckBox cbNotificacao;

    // variáveis que representam os checkbox's da activity
    protected CheckBox cbSegunda;
    protected CheckBox cbTerca;
    protected CheckBox cbQuarta;
    protected CheckBox cbQuinta;
    protected CheckBox cbSexta;
    protected CheckBox cbSabado;

    protected AdapterFormDia adapterFormDia;
    protected Toolbar myToolbar;

    protected Aviso aviso;
    protected DataBasePersistencia dao;
    //  protected DiaFragment diaFrament;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_disciplina);
        aviso = new Aviso(AdicaoActivity.this);
        dao = new DataBasePersistencia(this);
        disciplina = new Disciplina();

        localizarCampos();
        myToolbar.setTitle(R.string.adicionar_disciplina);
        adapterFormDia = new AdapterFormDia(disciplina.getAulas(), this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapterFormDia);

        marcarDiaEmExibicao();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_adicao, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (isAlgoPreenchido()) {
                    aviso.showDialogSair(R.string.descarte_alteracao);
                } else {
                    finish();
                }
                break;

            case R.id.action_salvar:
                aviso.showDialog(getString(R.string.armazenar_alteracao), R.string.sim, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        salvar();
                    }
                });
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        if (isAlgoPreenchido()) {
            aviso.showDialogSair(R.string.descarte_alteracao);
        } else {
            finish();
        }
    }


    /**
     * Método para monitorar os checkboxes e mudar a visibilidade dos layouts de acordo com o
     * que foi selecionado
     */
    public void onCheckboxClicked(View view) {
        boolean checked = ((CheckBox) view).isChecked();
        switch (view.getId()) {
            case R.id.checkBox_segunda:
                if (checked) {
                    adicionarDia(getString(R.string.segunda));
                } else {
                    removerDia(getString(R.string.segunda));
                }
                break;
            case R.id.checkBox_terca:
                if (checked) {
                    adicionarDia(getString(R.string.terca));
                } else {
                    removerDia(getString(R.string.terca));
                }
                break;
            case R.id.checkBox_quarta:
                if (checked) {
                    adicionarDia(getString(R.string.quarta));
                } else {
                    removerDia(getString(R.string.quarta));
                }
                break;
            case R.id.checkBox_quinta:
                if (checked) {
                    adicionarDia(getString(R.string.quinta));
                } else {
                    removerDia(getString(R.string.quinta));
                }
                break;
            case R.id.checkBox_sexta:
                if (checked) {
                    adicionarDia(getString(R.string.sexta));
                } else {
                    removerDia(getString(R.string.sexta));
                }
                break;
            case R.id.checkBox_sabado:
                if (checked) {
                    adicionarDia(getString(R.string.sabado));
                } else {
                    removerDia(getString(R.string.sabado));
                }
                break;
            case R.id.checkBox_notificacao_Edicao:
                showTimer();
        }
    }

    /**
     * Exibe a janela solicitando o horário para notificação
     */
    private void showTimer() {
        {
            if (cbNotificacao.isChecked()) {
                DialogFragment newFragment = new TimePickerFragment(cbNotificacao);
                FragmentTransaction ft = adapterFormDia.getContext().getSupportFragmentManager().beginTransaction();
                newFragment.show(ft, "TimePicker");
            }
        }
    }

    /**
     * Localiza todos os componentes gráficos
     */
    protected void localizarCampos() {
        // iniciar váriaveis
        nome = (EditText) findViewById(R.id.textInput_nome);
        professor = (EditText) findViewById(R.id.textInput_professor);
        turma = (EditText) findViewById(R.id.textInput_turma);
        semestre = (EditText) findViewById(R.id.textInput_semestre);
        curso = (AutoCompleteTextView) findViewById(R.id.textInput_curso);

        cbSegunda = (CheckBox) findViewById(R.id.checkBox_segunda);
        cbTerca = (CheckBox) findViewById(R.id.checkBox_terca);
        cbQuarta = (CheckBox) findViewById(R.id.checkBox_quarta);
        cbQuinta = (CheckBox) findViewById(R.id.checkBox_quinta);
        cbSexta = (CheckBox) findViewById(R.id.checkBox_sexta);
        cbSabado = (CheckBox) findViewById(R.id.checkBox_sabado);


        SharedPreferences settings = getSharedPreferences(getString(R.string.prefs), 0);

        if (!settings.getBoolean(getString(R.string.isUniversitario), true)) {
            findViewById(R.id.textInputLayout_curso).setVisibility(View.GONE);
            findViewById(R.id.textInputLayout_semestre).setVisibility(View.GONE);
        }

        cbNotificacao = (CheckBox) findViewById(R.id.checkBox_notificacao_Edicao);
        // auto complete de cursos
        String[] todosCursosUnipampa = {"Administração", "Agronegócio", "Agronomia", "Aquicultura ", "Biotecnologia", "Ciência da Computação", "Ciência e Tecnologia de Alimentos", "Ciências Biológicas", "Ciências da Natureza", "Ciências Econômicas", "Ciências Exatas", "Ciências Humanas", "Ciências Sociais – Ciência Política", "Direito", "Educação Física", "Enfermagem", "Eng. Agrícola", "Eng. Ambiental e Sanitária", "Eng. Civil", "Eng. de Agrimensura", "Eng. de Alimentos", "Eng. de Computação", "Eng. de Energia", "Eng. de Produção", "Eng. de Software", "Eng. de Telecomunicações", "Eng. Elétrica", "Eng. Florestal", "Eng. Mecânica", "Eng. Química", "Enologia", "Farmácia", "Física", "Fisioterapia", "Geofísica", "Geologia", "Gestão Ambiental", "Gestão de Turismo ", "Gestão Pública", "História", "Interdisciplinar em Ciências e Tecnologia", "Jornalismo", "Letras – Línguas Adicionais Inglês, Espanhol e Respectivas Literaturas", "Letras – Português", "Letras – Português e Espanhol", "Matemática", "Medicina", "Medicina Veterinária", "Mineração", "Música", "Nutrição", "Pedagogia", "Produção e Política Cultural", "Publicidade e Propaganda", "Química", "Relações Internacionais", "Relações Públicas", "Serviço Social", "Zootecnia"};
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.select_dialog_item, todosCursosUnipampa);
        curso.setThreshold(1);
        curso.setAdapter(arrayAdapter);

        myToolbar = (Toolbar) findViewById(R.id.toolbar_form);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView = (RecyclerView) findViewById(R.id.rvDias);
        View view = findViewById(R.id.activity_edicao);

        nome.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        nome.setRawInputType(InputType.TYPE_CLASS_TEXT);

        professor.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        professor.setRawInputType(InputType.TYPE_CLASS_TEXT);

    }

    /***
     * Irá marcar o checkbox correspondente ao dia que estava sendo exibido anteriormente
     */
    private void marcarDiaEmExibicao() {
        int dia = 0;
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            dia = bundle.getInt(getString(R.string.dia));
        }
        switch (dia) {
            case 2:
                cbSegunda.setChecked(true);
                adicionarDia(getString(R.string.segunda));
                break;
            case 3:
                cbTerca.setChecked(true);
                adicionarDia(getString(R.string.terca));
                break;
            case 4:
                cbQuarta.setChecked(true);
                adicionarDia(getString(R.string.quarta));
                break;
            case 5:
                adicionarDia(getString(R.string.quinta));
                cbQuinta.setChecked(true);
                break;
            case 6:
                adicionarDia(getString(R.string.sexta));
                cbSexta.setChecked(true);
                break;
            case 7:
                adicionarDia(getString(R.string.sabado));
                cbSabado.setChecked(true);
                break;
            default:
                break;
        }
    }


    /**
     * Salva os dados no banco de dados
     */
    private void salvar() {
        atualizaCamposDaDisciplina();
        if (!isDisciplinaDuplicada()) {
            if (isCamposObrigatoriosPreenchidos() && isAulasValidas()) {
                inserirAulasNoBanco();
                int mensagem;
                if (hasConflitoHorario()) {
                    mensagem = R.string.adicao_discilpina_conflito;
                } else {
                    mensagem = R.string.adicao_disciplina_sucesso;
                }
                aviso.toastMessager(mensagem);
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                finish();
            } else {
                aviso.snackBar(R.string.corrija_os_campos);
            }
        }
    }

    /**
     * Preenche os campos da disciplina em todas as aulas
     */
    protected void atualizaCamposDaDisciplina() {
        disciplina.setNome(nome.getText().toString());
        disciplina.setProfessor(professor.getText().toString());
        disciplina.setSemestre(semestre.getText().toString());
        disciplina.setCurso(curso.getText().toString());
        disciplina.setTurma(turma.getText().toString());

        // defindo o tempo a partir que foi definido
        if (cbNotificacao.isChecked()) {
            disciplina.setMinutosNotificar(TEMPO_NORIFICACAO);
        } else {
            disciplina.setMinutosNotificar(-1); // -1 significa sem notificação
        }
        // preenche a disciplina nas aulas
        ArrayList<Aula> aulas = (ArrayList<Aula>) adapterFormDia.getTodasAulas().clone();
        for (Aula aula : aulas) {
            aula.setDisciplina(disciplina);
        }
    }


    /**
     * Método para monitorar os checkboxes e mudar a visibilidade dos layouts de acordo com o
     * que foi selecionado
     */
    /**
     * Insere no banco de dados as aulas as aulas do dia informado
     * Agendar a notificação de uma determinada aula
     */
    protected void inserirAulasNoBanco() {
        NotificacaoController notificacaoController = new NotificacaoController(getApplicationContext());
        List<Aula> adicionadas = adapterFormDia.getAulasAdicionadas();
        for (Aula aula : adicionadas) {
            dao.insert(aula);
            if (cbNotificacao.isChecked()) {
                notificacaoController.agendarNotificacao(aula);
            }

        }
    }

    /**
     * Método que realiza a remoção das aulas do dia da lista
     *
     * @param string
     */
    protected void removerDia(String string) {
        adapterFormDia.removerDia(string);
    }

    /**
     * Método usado ao marcar um novo dia no checkbox, adiciona um campo
     *
     * @param dia
     */
    protected void adicionarDia(String dia) {
        adapterFormDia.adicionarDia(dia);
    }


    /// Validações ////

    /*
     * Método para verificar se todos os campos foram preenchidos, caso contrario seta uma mensagem
     * de erro no campo a ser preenchido
     */
    protected boolean isCamposObrigatoriosPreenchidos() {
        TextInputLayout inputNome = (TextInputLayout) findViewById(R.id.textInputLayout_nome);
        TextInputLayout inputDias = (TextInputLayout) findViewById(R.id.textInputLayout_dias);
        boolean tudook = true;

        if (contCheckboxChecked() == 0) {
            inputDias.setError(getString(R.string.selecionar_minimo_um_dia));
            inputDias.setErrorEnabled(true);
            cbSegunda.requestFocus();
            tudook = false;
        } else {
            inputDias.setError(null);
            cbSegunda.clearFocus();
        }

        if (inputNome.getEditText().getText().toString().trim().length() == 0) {
            inputNome.setError(getString(R.string.campo_vazio));
            inputNome.setErrorEnabled(true);
            nome.requestFocus();
            tudook = false;
        } else {
            nome.clearFocus();
            inputNome.setError(null);
        }
        return tudook;
    }


    /**
     * verifica se a aula adicionada é valida
     *
     * @return true se horário for válido
     */
    protected boolean isAulasValidas() {
        Aula aula = null;
        boolean isValido = true;
        for (int i = 0; i < adapterFormDia.getTodasAulas().size(); i++) {
            aula = adapterFormDia.getTodasAulas().get(i);
            if (!aula.isHorarioValido() &&
                    !adapterFormDia.getAulasRemovidas().contains(aula)) {
                adapterFormDia.exibirErroHora(aula.getDiaSemana());
                isValido = false;
            }
        }
        return isValido;
    }

    /**
     * Método que verifica se existe conflito de horário com alguma aula cadastrada no banco de dados
     *
     * @return true se houver conflito, false caso não exista
     */
    protected boolean hasConflitoHorario() {
        List<Aula> listaAulasBanco = dao.select();
        for (Aula aulaNova : disciplina.getAulas()) {
            for (Aula aulaCadastrada : listaAulasBanco) {
                if (aulaCadastrada.hasConflitoHorario(aulaNova)) {
                    return true;
                }
            }
        }
        return false;
    }


    /**
     * Método que verifica se existiu preenchimento de algum campo do formulário
     *
     * @return
     */
    public boolean isAlgoPreenchido() {
        String nome = this.nome.getText().toString().trim();
        String professor = this.professor.getText().toString().trim();
        String semestre = this.semestre.getText().toString().trim();
        String curso = this.curso.getText().toString().trim();
        String turma = this.turma.getText().toString().trim();

        ArrayList<Aula> aulas = adapterFormDia.getAulasAdicionadas();
        if (aulas.size() > 1)
            return true; // adicionou aula

        if (aulas.size() == 1 && (aulas.get(0).getSala().isEmpty() || aulas.get(0).isHorarioValido()))
            return true;

        return nome.length() > 0 ||
                professor.length() > 0 ||
                semestre.length() > 0 ||
                curso.length() > 0 ||
                turma.length() > 0 ||
                cbNotificacao.isChecked();

    }

    /**
     * Verifica se checkBox de notificação esta selecionado
     */
    protected boolean verificarNotificacao() {
        return (cbNotificacao.isChecked());
    }

    /**
     * Verifica se a disciplina já constava no banco
     * Caso sim irá apresentar a possibilidade de editar
     *
     * @return
     */
    public boolean isDisciplinaDuplicada() {
        final Disciplina disciplinaCadastrada = dao.selectDisciplina(disciplina.getNome());
        if (disciplinaCadastrada != null) {
            String mensagem = disciplina.getNome() + " " + getString(R.string.disciplina_ja_cadastrada);
            aviso.showDialog(mensagem, R.string.editar, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(getApplicationContext(), EdicaoActivity.class);
                    intent.putExtra(getString(R.string.adicionar_disciplina), (ArrayList<Aula>) disciplina.getAulas().clone());
                    disciplina.getAulas().addAll(disciplinaCadastrada.getAulas());
                    Aula.sort(disciplina.getAulas());
                    intent.putExtra(getString(R.string.disciplina), disciplina); // envia disciplina editada
                    getApplicationContext().startActivity(intent);
                }
            });
            return true;
        } else
            return false;
    }

    /**
     * Método para verificar se o usuário adicionou ou removeu alguma aula
     *
     * @return
     */
    private int contCheckboxChecked() {
        int cont = 0;
        if (cbSegunda.isChecked()) {
            cont++;
        }
        if (cbTerca.isChecked()) {
            cont++;
        }
        if (cbQuarta.isChecked()) {
            cont++;
        }
        if (cbQuinta.isChecked()) {
            cont++;
        }
        if (cbSexta.isChecked()) {
            cont++;
        }
        if (cbSabado.isChecked()) {
            cont++;
        }
        return cont;
    }

    /**
     * Recebe o nome do dia e desmarca o checkbox
     *
     * @param diaSemana
     */
    public void desmarcarChechBox(String diaSemana) {
        if (diaSemana.equals(getString(R.string.segunda))) {
            cbSegunda.setChecked(false);
        } else if (diaSemana.equals(getString(R.string.terca))) {
            cbTerca.setChecked(false);
        } else if (diaSemana.equals(getString(R.string.quarta))) {
            cbQuarta.setChecked(false);
        } else if (diaSemana.equals(getString(R.string.quinta))) {
            cbQuinta.setChecked(false);
        } else if (diaSemana.equals(getString(R.string.sexta))) {
            cbSexta.setChecked(false);
        } else if (diaSemana.equals(getString(R.string.sabado))) {
            cbSabado.setChecked(false);
        }
    }


    @SuppressLint("ValidFragment")
    public static class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
        private CheckBox cbNotificacao;


        public TimePickerFragment(CheckBox cbNotificacao) {
            this.cbNotificacao = cbNotificacao;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            boolean format = DateFormat.is24HourFormat(getActivity());

            int total = TempoUtils.extractTempoNotificacao(cbNotificacao.getHint().toString());
            int minuto = TempoUtils.extractMinutos(total);
            int hora = TempoUtils.extractHoras(total);

            TimePickerDialog time = new TimePickerDialog(getActivity(), R.style.DialogTheme, this, hora, minuto, format);
            time.setTitle(R.string.tempoNotificao);
            return time;
        }


        @Override
        public void onTimeSet(TimePicker view, int hora, int minuto) {
            String label = getString(R.string.receber_notificacao) + "" + TempoUtils.formatTempoNotificacao(hora, minuto) + "";
            cbNotificacao.setHint(label);
            TEMPO_NORIFICACAO = hora * 60 + minuto;
        }


    }
}