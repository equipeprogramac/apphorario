package br.edu.unipampa.apphorarios.persistencia;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import br.edu.unipampa.apphorarios.R;
import br.edu.unipampa.apphorarios.model.Aula;


public class ImportCSV {

    private AppCompatActivity context;
    private int colNome = -1, colDia = -1, colInicio = -1, colFim = -1, colSala = -1;
    private int colProfessor = -1, colSemestre = -1, colTurma = -1, colCurso = -1;
    private int colPredio = -1;

    public ImportCSV(AppCompatActivity context) {
        this.context = context;

    }

    private static final int READ_REQUEST_CODE = 42;

    public void importar() {
        verifyPermission();
    }

    /**
     * Abre intent para seleção de arquivo
     */
    public void openFile() {
        Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("text/csv");
        context.startActivityForResult(Intent.createChooser(intent, context.getString(R.string.escolha_o_arquivo_de_horarios)), READ_REQUEST_CODE);
    }


    /**
     * Verifica se existe a permissão para ler arquivos e caso não solicita ela.
     */
    public void verifyPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (context.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                ActivityCompat.requestPermissions(context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, READ_REQUEST_CODE);
            } else {
                openFile();
            }
        } else {
            openFile();
        }

    }


    /**
     * Realiza a importação das aulas do arquivo CSV para o banco de dados
     *
     * @param path
     * @return
     * @throws IOException
     */
    public ArrayList<Aula> importarAulas(String path) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(path), "iso-8859-1"));
        String line = reader.readLine();
        leituraCabecalho(line);
        ArrayList<Aula> aulas = new ArrayList<>();

        while ((line = reader.readLine()) != null) {
            String[] arrayString = line.split(";");
            // campos obrigatórios
            if (colNome != -1 && colDia != -1 && colInicio != -1 && colFim != -1 && arrayString.length > colFim) { // tratando disciplinas sem horário como TCC
                try {
                    String nomeDisciplina = formatarNome(arrayString[colNome]);
                    String diaSemana = fixDiaSemanaName(arrayString[colDia]);
                    String horaInicio = arrayString[colInicio].substring(0, 5);
                    String horaFim = arrayString[colFim].substring(0, 5);

                    Aula aula = new Aula();
                    aula.setNome(nomeDisciplina);
                    aula.setDiaSemana(diaSemana);
                    aula.setHoraInicio(horaInicio);
                    aula.setHoraTermino(horaFim);
                    aula.setMinutosNotificar(-1);

                    if (arrayString.length > colPredio) { // tratando enquanto a sala não foi cadastrada pela secretaria
                        String lab = "";   // TO-DO descobrir como é exibido outros tipos de Lab
                        if (!arrayString[colPredio].isEmpty() && arrayString[colPredio].contains("Laboratório de Informática")) { // pegando sala de láboratório
                            String[] split = arrayString[colPredio].split("Laboratório de Informática");
                            lab = "Lab" + split[1];
                        } else if (!arrayString[colPredio].isEmpty() && arrayString[colPredio].contains("Laboratório")) { // ex. Laboratório de Eletrotécnica
                            String[] split = arrayString[colPredio].split("Laboratório");
                            lab = "Laboratório " + split[1];
                        }
                        if (arrayString.length > colSala && !arrayString[colSala].isEmpty()) {
                            String sala = arrayString[colSala];
                            if (!lab.equals("")) {
                                lab += " (" + sala + ")";
                                aula.setSala(lab);
                            } else {
                                aula.setSala(sala);
                            }
                        }
                    }

                    // campos que não vem no csv padrão da unipampa
                    if (colProfessor != -1) {
                        aula.setProfessor(arrayString[colProfessor]);
                    }
                    if (colSemestre != -1) {
                        aula.setSemestre(arrayString[colSemestre]);
                    }
                    if (colCurso != -1) {
                        aula.setCurso(arrayString[colCurso]);
                    }
                    if (colTurma != -1) {
                        aula.setTurma(arrayString[colTurma]);
                    }
                    aulas.add(aula);
                } catch (Exception ignore) {
                }
            }
        }
        return aulas;
    }

    /**
     * Realiza a leitura do cabeçalho para extrair as colunas do csv
     *
     * @param linha
     */
    private void leituraCabecalho(String linha) {
        String[] array = linha.toLowerCase().split(";");
        for (int i = 0; i < array.length; i++) {
            switch (array[i].trim()) {
                case "componente curricular":
                    colNome = i;
                    break;
                case "dia":
                    colDia = i;
                    break;
                case "hora início":
                    colInicio = i;
                    break;
                case "hora fim":
                    colFim = i;
                    break;
                case "sala":
                    colSala = i;
                    break;
                case "prédio":
                    colPredio = i;
                    break;
                case "professor":
                    colProfessor = i;
                    break;
                case "semestre":
                    colSemestre = i;
                    break;
                case "turma":
                    colTurma = i;
                    break;
                case "curso":
                    colCurso = i;
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Formata nome deixando a apenas primeira letra de cada palavra maíuscula,
     * exceto conjunções, pronome e romanos
     *
     * @param nome
     * @return
     */
    private String formatarNome(String nome) {
        String[] trechos = nome.split(" ");
        nome = "";
        for (int i = 0; i < trechos.length; i++) {
            if (isConjuncaoOuPronome(trechos[i])) {
                nome += trechos[i].toLowerCase();
            } else if (isNumeroRomano(trechos[i])) {
                nome += trechos[i];
            } else {
                nome += trechos[i].substring(0, 1);
                nome += trechos[i].substring(1, trechos[i].length()).toLowerCase();
            }
            nome += " ";
        }
        return nome;
    }


    /**
     * Verifica se é número é romano
     *
     * @param string
     * @return
     */
    private boolean isNumeroRomano(String string) {
        return string.equals("I") || string.equals("II") || string.equals("III") || string.equals("IV") ||
                string.equals("V") || string.equals("VI") || string.equals("VII") || string.equals("VIII") ||
                string.equals("IX") || string.equals("X");
    }

    /**
     * Verifica se é conjunção ou Pronome
     *
     * @param string
     * @return
     */
    private boolean isConjuncaoOuPronome(String string) {
        return string.equals("E") || string.equals("OU") || string.equals("DE") || string.equals("DA") ||
                string.equals("NA") || string.equals("NO") || string.equals("POR") || string.equals("DO") ||
                string.equals("DOS") || string.equals("DAS") || string.equals("NAS") || string.equals("NOS") ||
                string.equals("A") || string.equals("O") || string.equals("AS") || string.equals("OS") ||
                string.equals("AO") || string.equals("AOS");
    }

    public String fixDiaSemanaName(String diaSemana) {
        String diaSemanaArrumado;
        int indexMinuscula = diaSemana.indexOf("-") + 1;
        diaSemanaArrumado = diaSemana.replace(diaSemana.charAt(indexMinuscula), Character.toUpperCase(diaSemana.charAt(indexMinuscula)));
        return diaSemanaArrumado;
    }


}
