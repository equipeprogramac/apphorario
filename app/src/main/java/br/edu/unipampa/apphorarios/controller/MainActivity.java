package br.edu.unipampa.apphorarios.controller;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import br.edu.unipampa.apphorarios.R;
import br.edu.unipampa.apphorarios.fragment.AvaliacaoFragment;
import br.edu.unipampa.apphorarios.fragment.ConfiguracaoFragment;
import br.edu.unipampa.apphorarios.fragment.DisciplinasFragment;
import br.edu.unipampa.apphorarios.fragment.HorariosFragment;
import br.edu.unipampa.apphorarios.fragment.SobreFragment;
import br.edu.unipampa.apphorarios.fragment.TutorialFragment;
import br.edu.unipampa.apphorarios.model.Aula;
import br.edu.unipampa.apphorarios.persistencia.DataBasePersistencia;
import br.edu.unipampa.apphorarios.persistencia.ImportCSV;
import br.edu.unipampa.apphorarios.util.ApiRate;
import br.edu.unipampa.apphorarios.util.RealPathUtil;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private static final int READ_REQUEST_CODE = 42;
    private ImportCSV importCsv;
    private HashMap<String, List<Aula>> mapDias;
    private NavigationView navigationView;
    private Aviso aviso;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        aviso = new Aviso(this);

        getSupportActionBar().setIcon(R.drawable.logo_toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        preencherMapDias();

        marcarMenu(1); // index de horários

        if (ApiRate.isVerificado == false && ApiRate.decidirAvaliar(this)) { // solicitar avaliação do apk se atender aos parametros
            //AvaliacaoFragment av = (AvaliacaoFragment) getSupportFragmentManager().findFragmentByTag(getString(R.string.avaliar_aplicativo));
          //  if (av == null || !av.isVisible()) { // não funcionou
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        perguntarSeGostou();
                    }
                }, 5000);
            }

        decidirTelaExibir();
        decidirExibirImportacao();
    }

    /**
     * Exibe mensagem perguntando se gostou do app, caso sim irá solicitar avaliação,
     * caso não pede feedback
     */
    private void perguntarSeGostou() {
        final android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        final AvaliacaoFragment aFragment = new AvaliacaoFragment();
        final Bundle args = new Bundle();


        AlertDialog.Builder builder = aviso.buildDialog(getString(R.string.esta_gostando_do_app), R.string.sim, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                args.putBoolean(getString(R.string.avaliar), true);
                aFragment.setArguments(args);
                aFragment.show(ft, getString(R.string.avaliar_aplicativo));
            }
        });
        builder.setNegativeButton(getString(R.string.nao), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                args.putBoolean(getString(R.string.avaliar), false);
                aFragment.setArguments(args);
                aFragment.show(ft, getString(R.string.avaliar_aplicativo));
            }
        });
        builder.setNeutralButton(null, null);
        builder.show();
    }

    /**
     * Caso tenha recebido bundle contendo disciplina irá exibi-la, caso contrário não
     */
    private void decidirTelaExibir() {
        Bundle b = getIntent().getExtras();
        if (b != null && b.containsKey(getString(R.string.disciplina))) {
            exibirFragment(new DisciplinasFragment(), getString(R.string.disciplinas));
            b.clear();
            getIntent().getExtras().clear();
        } else {
            exibirFragment(new HorariosFragment(), getString(R.string.meus_horarios));
        }
    }

    /**
     * Verifica se é aluno universitário para exibir a opção de importação
     */
    public void decidirExibirImportacao() {
        int indexImportar = 2; // index do importar no menu
        if (isAlunoUnipampa() && isUniversitario()) {
            navigationView.getMenu().getItem(indexImportar).setVisible(true);
        } else {
            navigationView.getMenu().getItem(indexImportar).setVisible(false);
        }

    }

    /**
     * Verifica se a exibição de curso e semestre estão habilitadas
     *
     * @return
     */
    public boolean isUniversitario() {
        SharedPreferences settings = getSharedPreferences(getString(R.string.prefs), 0);
        return settings.getBoolean(getString(R.string.isUniversitario), true);
    }


    /**
     * Verifica se o módulo de importação está hábilitado
     *
     * @return
     */
    public boolean isAlunoUnipampa() {
        SharedPreferences settings = getSharedPreferences(getString(R.string.prefs), 0);
        return settings.getBoolean(getString(R.string.isAlunoUnipampa), true);
    }

    public void marcarMenu(int index) {
        navigationView.getMenu().getItem(index).setChecked(true);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            int count = getFragmentManager().getBackStackEntryCount();
            if (count == 0) {
                super.onBackPressed();
            } else {
                getFragmentManager().popBackStack();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // MenuInflater inflater = getMenuInflater();
        // inflater.inflate(R.menu.menu_apagar_tudo, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (gerenciarOpcoes(item)) {
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        gerenciarOpcoes(item);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * Método utilizado para gerenciar as respostas do menu
     *
     * @param item
     * @return
     */
    private boolean gerenciarOpcoes(MenuItem item) {
        Fragment fragment = null;
        String tag = "";
        switch (item.getItemId()) {
            case R.id.sobreApk:
                fragment = new SobreFragment();
                break;
            case R.id.meusHorarios:
                tag = getString(R.string.meus_horarios);
                fragment = new HorariosFragment();
                break;
            case R.id.disciplinas:
                tag = getString(R.string.disciplinas);
                fragment = new DisciplinasFragment();
                break;
            case R.id.configuracao:
                tag = getString(R.string.configuracoes);
                fragment = new ConfiguracaoFragment();
                break;
            case R.id.importarCSV:
                this.exibeMensagemImportacao(true);
                break;
        }
        if (fragment != null) {
            exibirFragment(fragment, tag);
        }

        //progressBar.setVisibility(View.GONE);
        return true;
    }

    /**
     * retorna as aulas do dia passado por parametro
     *
     * @param dia
     * @return
     */
    public List<Aula> getAulaDia(String dia) {
        return mapDias.get(dia);
    }

    /**
     * Popula o map com as aulas dos dias da semana
     */
    public void preencherMapDias() {
        mapDias = new HashMap<>();
        mapDias.put(getString(R.string.segunda), new ArrayList<Aula>());
        mapDias.put(getString(R.string.terca), new ArrayList<Aula>());
        mapDias.put(getString(R.string.quarta), new ArrayList<Aula>());
        mapDias.put(getString(R.string.quinta), new ArrayList<Aula>());
        mapDias.put(getString(R.string.sexta), new ArrayList<Aula>());
        mapDias.put(getString(R.string.sabado), new ArrayList<Aula>());

        List<Aula> aulas = new DataBasePersistencia(this).select();
        for (Aula aula : aulas) {
            mapDias.get(aula.getDiaSemana()).add(aula);
        }

        Aula.sort(mapDias.get(getString(R.string.segunda)));
        Aula.sort(mapDias.get(getString(R.string.terca)));
        Aula.sort(mapDias.get(getString(R.string.quarta)));
        Aula.sort(mapDias.get(getString(R.string.quinta)));
        Aula.sort(mapDias.get(getString(R.string.sexta)));
        Aula.sort(mapDias.get(getString(R.string.sabado)));
    }


    /**
     * Altera a view para  fragment selecionado
     *
     * @param fragment
     * @param tag
     */
    private void exibirFragment(Fragment fragment, String tag) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, fragment, tag);
        ft.commit();
    }

    /**
     * Esse método irá informar o usuário sobre a importação e após abre para seleção
     */
    public void exibeMensagemImportacao(boolean showTutorial) {
        AlertDialog.Builder builder = aviso.buildDialog(getString(R.string.informa_importacao_csv), R.string.selecionar, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                importarCSV();
            }
        });
        if (showTutorial) {
            builder.setNeutralButton(R.string.tutorial, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    exibirFragment(new TutorialFragment(), getString(R.string.tutorial));
                }
            });
        }
        builder.show();
    }


    public void importarCSV() {
        importCsv = new ImportCSV(this);
        importCsv.importar();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {

        if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                avisoPermissao();
            } else {
                Uri uri = null;
                if (resultData != null) {
                    uri = resultData.getData();
                    String path = RealPathUtil.getRealPath(this, uri);

                    if (path.endsWith(".csv")) {
                        importarAulas(path);
                    } else {
                        avisoErroImportacao(R.string.arquivo_nao_e_csv);
                    }


                }
            }
        }
    }

    private void importarAulas(String path) {
        try {
            final ArrayList<Aula> aulas = importCsv.importarAulas(path);
            if (aulas != null && aulas.size() > 0) { // se existem aulas novas

                DataBasePersistencia dao = new DataBasePersistencia(this);
                if (dao.select().size() == 0) { // não  existiam aulas cadastradas, adiciona direto
                    new AdicionarAulas(aulas, false).execute();
                } else {   // questiona se deseja apagar aulas anteriores e depois adiciona
                    AlertDialog.Builder builder = aviso.buildDialog(getString(R.string.apagar_aulas_anteriores), R.string.sim, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            new AdicionarAulas(aulas, true).execute();
                        }
                    });
                    builder.setNegativeButton(R.string.nao, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            new AdicionarAulas(aulas, false).execute();
                        }
                    });
                    builder.setCancelable(false);
                    builder.setNeutralButton(null, null);
                    builder.show();
                }
            } else {
                avisoErroImportacao(R.string.nenhuma_aula_importada);
            }
        } catch (IOException e) {
        }
    }

    /**
     * Exibe um snackbar com informado a necessidade de permissão para importação
     */
    private void avisoPermissao() {
        aviso.snackBar(R.string.aviso_permissao, R.string.configuracoes, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivityForResult(intent, READ_REQUEST_CODE);
            }
        });
    }

    /**
     * Exibe um snackbar com informado erro na importação csv
     *
     * @param id id da mensagem
     */
    private void avisoErroImportacao(int id) {
        aviso.snackBar(id, R.string.tutorial, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exibirFragment(new TutorialFragment(), getString(R.string.tutorial));
            }
        });
    }

    public void confirmaLimparBanco() {
        aviso.showDialog(getString(R.string.aviso_exclusao), R.string.sim, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                new MainActivity.LimpaBanco().execute();
            }
        });
    }

    private class LimpaBanco extends AsyncTask<String, Void, Void> {
        private ProgressDialog load;

        @Override
        protected void onPreExecute() {
            load = ProgressDialog.show(MainActivity.this, "Apagando...",
                    "Excluindo aulas cadastradas...");
        }

        @Override
        protected Void doInBackground(String... params) {
            DataBasePersistencia dao = new DataBasePersistencia(getApplicationContext());
            dao.deleteAll();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            load.dismiss();
            preencherMapDias();
            Snackbar.make(findViewById(android.R.id.content), R.string.todas_aulas_apagadas, Snackbar.LENGTH_LONG).show();

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case READ_REQUEST_CODE: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    importarCSV();

                } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {//&& !shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    avisoPermissao();
                }
            }
        }
    }

    private class AdicionarAulas extends AsyncTask<String, Void, Void> {
        private ProgressDialog load;
        private ArrayList<Aula> aulas;
        private boolean apagarAulas;

        public AdicionarAulas(ArrayList<Aula> aulas, boolean apagarAulas) {
            this.aulas = aulas;
            this.apagarAulas = apagarAulas;
        }

        @Override
        protected void onPreExecute() {
            load = ProgressDialog.show(MainActivity.this, "Adicionando...",
                    "Adicionando aulas aulas cadastradas...");
        }

        @Override
        protected Void doInBackground(String... params) {
            DataBasePersistencia dao = new DataBasePersistencia(getApplicationContext());
            if (apagarAulas) {
                dao.deleteAll();
            }
            for (Aula aula : aulas) {
                dao.insert(aula);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            load.dismiss();
            aviso.toastMessager(R.string.importacao_concluida);
            Intent intent = new Intent(MainActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }

    }

    public Aviso getAviso() {
        return aviso;
    }
}


