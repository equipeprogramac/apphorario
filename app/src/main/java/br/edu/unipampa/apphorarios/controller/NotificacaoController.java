package br.edu.unipampa.apphorarios.controller;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.annotation.NonNull;

import java.util.List;

import br.edu.unipampa.apphorarios.R;
import br.edu.unipampa.apphorarios.model.Aula;
import br.edu.unipampa.apphorarios.persistencia.DataBasePersistencia;
import br.edu.unipampa.apphorarios.service.AlarmReceiver;
import br.edu.unipampa.apphorarios.service.NotificacaoJobService;
import br.edu.unipampa.apphorarios.util.TempoUtils;


public class NotificacaoController {
    private Context context;
    private DataBasePersistencia dao;

    public NotificacaoController(Context context) {
        this.context = context;
        this.dao = new DataBasePersistencia(context);
    }


    /**
     * Exibe a notificação para o usuário
     *
     * @param aula
     */
    public void showNotificacao(Aula aula) {
        if (aula != null) {
            prepararnotificacao(aula, true);
        }
    }

    public void showNotificacao(int id) {
        for (Aula aula : dao.selectComNotificacao()) {
            if (aula.getId() == id) {
                prepararnotificacao(aula, true);
            }
        }

    }

    /**
     * Cria uma notificação com as informações da aula
     *
     * @param aula
     * @param isAgora notificar a aula ou não
     * @return Notification construida
     */
    public Notification.Builder prepararnotificacao(Aula aula, boolean isAgora) {
        int notificacaioId = aula.getId();
        Notification.Builder mBuilder;
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        // Inicializar channel para Oreo
        String CHANNEL_ID = context.getResources().getString(R.string.app_name);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, CHANNEL_ID, NotificationManager.IMPORTANCE_HIGH);
            mNotificationManager.createNotificationChannel(mChannel);
            mBuilder = new Notification.Builder(context, CHANNEL_ID);
        } else {
            mBuilder = new Notification.Builder(context)
                    .setDefaults(Notification.PRIORITY_HIGH);
        }
        mBuilder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher));
        mBuilder.setSmallIcon(R.drawable.ic_logo);

        mBuilder.setContentTitle(aula.getNome());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBuilder.setColor(context.getResources().getColor(R.color.colorPrimaryDark));
            mBuilder.setVisibility(Notification.VISIBILITY_PUBLIC);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mBuilder.setBadgeIconType(Notification.BADGE_ICON_SMALL);
        }
        mBuilder.setContentText(aula.textoNotificacao());
        mBuilder.setDefaults(Notification.DEFAULT_ALL);

        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent notificacaoIntent = PendingIntent.getActivity(context, notificacaioId, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        mBuilder.setContentIntent(notificacaoIntent);
        mBuilder.setAutoCancel(true);
        if (isAgora) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                mNotificationManager.notify(aula.getId(), mBuilder.build());
            } else {
                mNotificationManager.notify(aula.getId(), mBuilder.getNotification());
            }
        }
        return mBuilder;
    }

    /**
     * Agenda todas as notificações de aula
     */
    public void prepararNotificacoes() {
        AlarmReceiver alarmReceiver = new AlarmReceiver();
        // agenda todas as notificações de aula
        for (Aula aula : dao.selectComNotificacao()) {
            alarmReceiver.agendarAlarme(aula, context); // agenda alarme independente da versão do android
        }

        // prepara o job que irá garantir a chamada da próxima aula
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // agenda o job apenas se o horário for novo for antes do antigo job
            new NotificacaoJobService().agendarProximoJob(this, context);
        }
    }

    /**
     * Prepara o alarme da aula criando o alarm e o job caso seja superior a versão 5.0
     *
     * @param aula
     */
    public void agendarNotificacao(@NonNull Aula aula) {
        new AlarmReceiver().agendarAlarme(aula, context); // agenda alarme independente da versão do android

        // prepara o job que irá garantir a chamada da próxima aula
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // agenda o job apenas se o horário for novo for antes do antigo job
            new NotificacaoJobService().agendarProximoJob(this, context);
        }
    }

    /**
     * Cancela o alarme da notificação
     *
     * @param aula
     */
    public void cancelarNotificacao(Aula aula) {
        int id = aula.getId();
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context.getApplicationContext(), AlarmReceiver.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(context.getApplicationContext(), id, intent, 0);
        alarmManager.cancel(alarmIntent);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            new NotificacaoJobService().removerJob(aula, context, this);
        }
    }

    /**
     * Busca a próxima aula que deve ser notificada
     *
     * @return
     */
    public Aula getProximaAulaNotificacao() {
        List<Aula> aulasNotificacao = dao.selectComNotificacao();

        Aula proxAula = null;
        long notProxAula = Long.MAX_VALUE;

        long notTemp;
        for (Aula tempAula : aulasNotificacao) {
            notTemp = TempoUtils.millisTempoNotificacao(tempAula);
            if (notTemp < notProxAula) { // encontrou uma mais próxima
                proxAula = tempAula;
                notProxAula = notTemp;
            }
        }
        return proxAula;
    }

    /**
     * Retorna a aula seguinte a que foi passada por parametro
     *
     * @param aulaAnterior
     * @return caso não exista outra aula com notificação
     */
    public Aula getNotificacaoSeguinte(@NonNull Aula aulaAnterior) {
        List<Aula> aulasNotificacao = dao.selectComNotificacao();

        long notAulaAnterior = TempoUtils.millisTempoNotificacao(aulaAnterior);

        Aula proxAula = null; // inicializa aleatoriamente
        long notProxAula = Long.MAX_VALUE;

        long notTemp;
        for (Aula tempAula : aulasNotificacao) {
            notTemp = TempoUtils.millisTempoNotificacao(tempAula);

            if (notTemp < notProxAula) { // encontrou uma mais próxima
                if (notAulaAnterior < notTemp) { // a próxima deve ser depois da aula anterior
                    proxAula = tempAula;
                    notProxAula = notTemp;
                }
            }
        }
        return proxAula;
    }
}

