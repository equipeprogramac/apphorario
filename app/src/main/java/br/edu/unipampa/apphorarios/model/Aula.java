package br.edu.unipampa.apphorarios.model;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.edu.unipampa.apphorarios.util.TempoUtils;


public class Aula implements Serializable {

    private String horaInicio;
    private String horaTermino;
    private String sala;
    private String diaSemana;
    private int id;
    private Disciplina disciplina;

    public Aula() {
        this.horaInicio = "00:00";
        this.horaTermino = "00:00";
        this.sala = "";
        this.diaSemana = "";
        this.disciplina = new Disciplina();
        this.disciplina.add(this);
    }

    public Aula(String diaSemana) {
        this.horaInicio = "00:00";
        this.horaTermino = "00:00";
        this.sala = "";
        this.diaSemana = diaSemana;
        this.disciplina = new Disciplina();
        this.disciplina.add(this);
    }

    public Aula(Disciplina disciplina) {
        this.horaInicio = "00:00";
        this.horaTermino = "00:00";
        this.sala = "";
        this.diaSemana = "";
        this.disciplina = disciplina;
        this.disciplina.add(this);
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String hora) {
        this.horaInicio = hora;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }

    public String getDiaSemana() {
        return diaSemana;
    }

    public void setDiaSemana(String diaSemana) {
        this.diaSemana = diaSemana;
    }

    public String getHoraTermino() {
        return horaTermino;
    }

    public void setHoraTermino(String horaTermino) {
        this.horaTermino = horaTermino;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public void setDisciplina(Disciplina disciplina) {
        this.disciplina = disciplina;
        disciplina.add(this); // só adiciona se ela ainda não estiver sido adicionada
    }

    /// campos da disciplna
    public void setMinutosNotificar(int valor) {
        this.disciplina.setMinutosNotificar(valor);
    }


    public void setCurso(String curso) {
        this.disciplina.setCurso(curso);
    }

    public String getCurso() {
        return this.disciplina.getCurso();
    }

    public void setSemestre(String semestre) {
        this.disciplina.setSemestre(semestre);
    }

    public String getSemestre() {
        return this.disciplina.getSemestre();

    }

    public String getNome() {
        return this.disciplina.getNome();
    }

    public void setNome(String nome) {
        this.disciplina.setNome(nome);
    }

    public String getProfessor() {
        return this.disciplina.getProfessor();
    }

    public void setProfessor(String professor) {
        this.disciplina.setProfessor(professor);
    }


    public String getTurma() {
        return this.disciplina.getTurma();
    }

    public void setTurma(String turma) {
        this.disciplina.setTurma(turma);
    }

    @Override
    public String toString() {
        return "Aula{" +
                "nome= '" + disciplina.getNome() + '\'' +
                ", diaSemana:'" + diaSemana + '\'' +
                ", notificar ='" + disciplina.getMinutosNotificar() + '\'' +
                ", horaInicio='" + horaInicio + '\'' +
                ", horaTermino='" + horaTermino + '\'' +
                ", sala='" + sala + '\'' +
                ", professor='" + disciplina.getProfessor() + '\'' +
                ", turma='" + disciplina.getTurma() + '\'' +
                ", curso='" + disciplina.getSemestre() + '\'' +
                ", semestre='" + disciplina.getSemestre() + '\'' +
                '}';
    }

    public String textoNotificacao() {
        return "Ás " + horaInicio + ", sala " + sala + ".";
    }

    public Disciplina getDisciplina() {
        return disciplina;
    }

    /**
     * Verifica se os campos obrigatórios estão validados
     *
     * @return
     */
    public boolean isValida() {
        return !disciplina.getNome().isEmpty() && !diaSemana.isEmpty() && !horaTermino.isEmpty() && !horaInicio.isEmpty() && isHorarioValido();
    }

    /**
     * Verifica se o o término é antes do início
     *
     * @return
     */
    public boolean isHorarioValido() {
        long inicio = TempoUtils.extractTotalMinutos(horaInicio);
        long fim = TempoUtils.extractTotalMinutos(horaTermino);
        return fim > inicio;
    }

    /**
     * Método que compara duas aulas para verificar se existe alguma diferença
     *
     * @param aula2
     * @return
     */
    public boolean isIgual(Aula aula2) {
        if (aula2 == null) {
            return false;
        } else {
            return sala.equals(aula2.getSala()) &&
                    horaInicio.equals(aula2.getHoraInicio()) &&
                    horaTermino.equals(aula2.getHoraTermino());
            //   disciplina.equals(aula2.getDisciplina());
        }
    }

    /**
     * Determina o horário para notificar a aula
     *
     * @return horário para notificar em minutos
     */
    public int getMinutosNotificar() {
       return disciplina.getMinutosNotificar();
    }

    /**
     * Método que compara a aula atual e a outra aulapara verificar se elas tem conflito de horário
     *
     * @param aula2
     * @return retorna true se houver conflito, false caso contrario
     */
    public boolean hasConflitoHorario(Aula aula2) {
        if (getDiaSemana().equals(aula2.getDiaSemana())) {
            long aula1Inicio = TempoUtils.extractTotalMinutos(horaInicio);
            long aula1Final = TempoUtils.extractTotalMinutos(horaTermino);

            long aula2Inicio = TempoUtils.extractTotalMinutos(aula2.getHoraInicio());
            long aula2Final = TempoUtils.extractTotalMinutos(aula2.getHoraTermino());
            // identifica qual aula começa primeiro
            if (aula1Inicio < aula2Inicio) { // aula1 começa depois da aula2
                return aula1Final > aula2Inicio; // aula2 precisa começar depois do termino da aula1
            } else if (aula2Inicio < aula1Inicio) {
                return aula2Final > aula1Inicio;
            } else { // ambas iniciam no mesmo horário
                if (id == aula2.getId()) // são a mesma aula
                    return false;
                else
                    return aula1Inicio == aula2Inicio;
            }
        } else {
            return false;
        }
    }
    /**
     * Organiza as aulas por dia, hora de início e de término
     *
     * @param listaAulas
     */
    public static void sort(List<Aula> listaAulas) {
        Collections.sort(listaAulas, new Comparator<Aula>() {
            @Override
            public int compare(Aula o1, Aula o2) {
                int tempo1 = TempoUtils.extractValorNumericoDeDia(o1.getDiaSemana());
                int tempo2 = TempoUtils.extractValorNumericoDeDia(o2.getDiaSemana());
                if (tempo1 < tempo2) {   // ordena por dia
                    return -1;
                } else if (tempo1 > tempo2) {
                    return 1;
                } else { // se dia igual ordena por horá início
                    tempo1 = TempoUtils.extractTotalMinutos(o1.getHoraInicio());
                    tempo2 = TempoUtils.extractTotalMinutos(o2.getHoraInicio());
                    if (tempo1 < tempo2) {
                        return -1;
                    } else if (tempo1 > tempo2) {
                        return 1;
                    } else {  // se o horário de ínicio é igual ordena  horário término
                        tempo1 = TempoUtils.extractTotalMinutos(o1.getHoraTermino());
                        tempo2 = TempoUtils.extractTotalMinutos(o2.getHoraTermino());
                        if (tempo1 < tempo2) {
                            return -1;
                        } else if (tempo1 > tempo2) {
                            return 1;
                        } else {
                            return 0;
                        }
                    }
                }
            }
        });
    }
}
