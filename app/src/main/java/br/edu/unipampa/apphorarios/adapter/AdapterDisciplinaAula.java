package br.edu.unipampa.apphorarios.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import java.util.ArrayList;

import br.edu.unipampa.apphorarios.R;
import br.edu.unipampa.apphorarios.model.Aula;
import br.edu.unipampa.apphorarios.util.TextViewUtils;


public class AdapterDisciplinaAula extends RecyclerView.Adapter<AdapterDisciplinaAula.EditViewHolder> {
    private ArrayList<Aula> aulasDisciplina;
    private Context context;

    public AdapterDisciplinaAula(ArrayList<Aula> aulasDisciplina, Context context) {
        this.aulasDisciplina = aulasDisciplina;
        this.context = context;
    }

    //// INICIO VIEWHOLDER //

    public class EditViewHolder extends RecyclerView.ViewHolder {

        private TextView tvDiaSemana;
        private View view;

        private AdapterView.OnItemSelectedListener listener;

        public EditViewHolder(View view) {
            super(view);
            this.tvDiaSemana = (TextView) view.findViewById(R.id.diaSemana);
            this.view = view;
        }
    }
    //// FIM VIEWHOLDER //


    @Override
    public EditViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_disciplina_aula, parent, false);
        return new EditViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final EditViewHolder holder, final int position) {
        if (aulasDisciplina.size() > position) {
            Aula aula = aulasDisciplina.get(position);

            String exibicao = aula.getDiaSemana() + ": " + aula.getHoraInicio() + " às " + aula.getHoraTermino();
            if (!aula.getSala().isEmpty()) {
                exibicao += " / " + aula.getSala();
            }
            holder.tvDiaSemana.setText(exibicao);
            TextViewUtils.editarEstilo(holder.tvDiaSemana);
        }
    }


    @Override
    public int getItemCount() {
        return aulasDisciplina.size();
    }

}







