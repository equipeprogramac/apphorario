package br.edu.unipampa.apphorarios.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import br.edu.unipampa.apphorarios.R;
import br.edu.unipampa.apphorarios.adapter.AdapterHorario;
import br.edu.unipampa.apphorarios.controller.MainActivity;
import br.edu.unipampa.apphorarios.model.Aula;

public class HorariosDiaFragment extends Fragment implements View.OnClickListener {


    private View rootView;
    private RecyclerView recyclerView;
    private String dia;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_horarios_dia, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.list);


        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.dia))) {
                dia = bundle.getString(getString(R.string.dia));
            }
        }

        recyclerView.setOnClickListener(this);

        preencherLista();
        return rootView;
    }

    /**
     * Preenche a lista com as aulas do dia contidas na main
     */
    private void preencherLista() {
        List<Aula> aulas = ((MainActivity) getActivity()).getAulaDia(dia);
        AdapterHorario adapter = new AdapterHorario(aulas, rootView.getContext());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);

        TextView emptyView = (TextView) rootView.findViewById(R.id.empty_view);
        if (aulas.isEmpty()) {
            emptyView.setVisibility(View.VISIBLE);
        } else {
            emptyView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(getActivity(), DetalhesHorarioFragment.class);
        startActivity(intent);

    }


}
