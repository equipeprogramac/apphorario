package br.edu.unipampa.apphorarios.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import br.edu.unipampa.apphorarios.R;
import br.edu.unipampa.apphorarios.util.TextViewUtils;


public class SobreFragment extends DialogFragment implements View.OnTouchListener, View.OnClickListener {


    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_sobre, container, false);
        TextView tvSobre = view.findViewById(R.id.sobreApp);

        // exibe informação sobre a versão do Apk
        String sobre = tvSobre.getText().toString();
        try {
            PackageInfo packageInfo = getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0);
            sobre += " " + packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            sobre += " 2.0";
            e.printStackTrace();
        }
        sobre += ".";
        tvSobre.setText(sobre);


        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        int width = size.x / 2 - 20;
        final ImageView logoUnipampa = view.findViewById(R.id.logoUnipampa);
        logoUnipampa.setMaxWidth(width);
        logoUnipampa.setMaxHeight(width / 2);
        logoUnipampa.setOnClickListener(this);

        width = size.x / 2 - 5;
        final ImageView programaC = view.findViewById(R.id.logoProgramaC);
        programaC.setMaxWidth(width);
        programaC.setOnClickListener(this);

        // set link para página
        TextView tvVisite = ((TextView) view.findViewById(R.id.visite_pagina));
        tvVisite.setPaintFlags(tvVisite.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG); // habilitar sublinhado
        tvVisite.setTextColor(getResources().getColor(R.color.colorAccent));
        tvVisite.setOnClickListener(this);

        Button btAvaliar = view.findViewById(R.id.btAvaliar);
        btAvaliar.setOnClickListener(this);


        Button btFeedback = view.findViewById(R.id.btFeedback);
        btFeedback.setOnClickListener(this);

        logoUnipampa.setOnTouchListener(this);
        programaC.setOnTouchListener(this);

        TextViewUtils.changeIconColor(btAvaliar, R.color.icone);
        TextViewUtils.changeIconColor(btFeedback, R.color.icone);
        return view;
    }

    private void abrirPaginaProgramaC() {
        String programacURL = "https://www.facebook.com/PaginaProgramaC";
        String programacID = "PaginaProgramaC"; // ou 793919484042856 testar com as diferentes versões
        String url;
        PackageManager packageManager = getContext().getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
            if (versionCode >= 3002850) { //  versões do fb superiores a  11.0.0.11.23
                url = "fb://facewebmodal/f?href=" + programacURL;
            } else { // versões anteriores
                url = "fb://page/" + programacID;
            }
        } catch (PackageManager.NameNotFoundException e) {
            url = programacURL; // link normal
        }
        Uri uri = Uri.parse(url);

        Intent link = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(link);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btAvaliar) {
            AvaliacaoFragment.avaliarAplicativo(getContext());
        } else if (id == R.id.visite_pagina || id == R.id.logoProgramaC) {
            abrirPaginaProgramaC();
        } else if (id == R.id.logoUnipampa) {
            Intent link = new Intent(Intent.ACTION_VIEW, Uri.parse("http://novoportal.unipampa.edu.br/novoportal/"));
            startActivity(link);
        } else if (id == R.id.btFeedback) {
            AvaliacaoFragment.abrirFormFeedback(getContext());
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        // dando efeito de escurecer a cor da imagem ao clicar
        if (v instanceof ImageView) {
            ImageView view = (ImageView) v;
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN: {
                    //camada preta com transparencia of 0x77 (119)
                    view.getDrawable().setColorFilter(0x77000000, PorterDuff.Mode.SRC_ATOP);
                    view.invalidate();
                    break;
                }
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL: {
                    //limpa camada
                    view.getDrawable().clearColorFilter();
                    view.invalidate();
                    break;
                }
            }

        }
        return false;
    }
}
