package br.edu.unipampa.apphorarios.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import br.edu.unipampa.apphorarios.R;
import br.edu.unipampa.apphorarios.util.TextViewUtils;


public class AvaliacaoFragment extends DialogFragment implements View.OnClickListener {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.DialogAnimation;
        getDialog().setCanceledOnTouchOutside(true);

        View view = inflater.inflate(R.layout.fragment_avaliacao, container, false);

        TextView btAvaliar = view.findViewById(R.id.btAvaliar);
        btAvaliar.setOnClickListener(this);

        TextView btFeedback = view.findViewById(R.id.btFeedback);
        btFeedback.setOnClickListener(this);

        TextView btDepois = view.findViewById(R.id.btDepois);
        btDepois.setOnClickListener(this);

        TextView btNaoMostrar = view.findViewById(R.id.btNaoMostrar);
        btNaoMostrar.setOnClickListener(this);

        TextViewUtils.changeIconColor(btAvaliar, R.color.icone);
        TextViewUtils.changeIconColor(btDepois, R.color.icone);
        TextViewUtils.changeIconColor(btNaoMostrar, R.color.icone);
        TextViewUtils.changeIconColor(btFeedback, R.color.icone);

        boolean isAvialiacao = getArguments().getBoolean(getString(R.string.avaliar));
        if (isAvialiacao) {
            btFeedback.setVisibility(View.GONE);
        } else {
            btAvaliar.setVisibility(View.GONE);
            btFeedback.setVisibility(View.VISIBLE);

            TextView tvTitulo = view.findViewById(R.id.tituloAvalicacao);
            tvTitulo.setText(R.string.sentimos_muito);
            TextView tvTexto = view.findViewById(R.id.textoAvalicao);
            tvTexto.setText(R.string.queremos_saber);

        }

        return view;
    }


    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }

    @Override
    public void onClick(View v) {
        SharedPreferences prefs = getContext().getSharedPreferences(getContext().getString(R.string.prefs), 0);
        SharedPreferences.Editor editor = prefs.edit();

        int id = v.getId();
        if (id == R.id.btAvaliar) {
            avaliarAplicativo(getActivity());
            editor.putBoolean(getContext().getString(R.string.dontShowAgain), true);
        } else if (id == R.id.btFeedback) {
            AvaliacaoFragment.agradecerFeedback(getContext());
            editor.putBoolean(getContext().getString(R.string.dontShowAgain), true);
        } else if (id == R.id.btNaoMostrar) {
            editor.putBoolean(getContext().getString(R.string.dontShowAgain), true);
        }

        editor.apply();
        this.dismiss();
    }

    /**
     * Inicia a avaliação do aplicativo
     *
     * @param context
     */
    public static void avaliarAplicativo(Context context) {

        String url = "";
        try {
            url = "market://details";
        } catch (ActivityNotFoundException e) {
            url = "https://play.google.com/store/apps/details";
        }

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("%s?id=%s", url, context.getPackageName())));
        int flags = Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_MULTIPLE_TASK;
        if (Build.VERSION.SDK_INT >= 21) {
            flags |= Intent.FLAG_ACTIVITY_NEW_DOCUMENT;
        } else {
            //noinspection deprecation
            flags |= Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET;
        }
        intent.addFlags(flags);
        context.startActivity(intent);

        AvaliacaoFragment.exibirAgradecimentoAvaliacao(context);
    }

    public static void abrirFormFeedback(Context context) {
        Intent link = new Intent(Intent.ACTION_VIEW, Uri.parse("https://goo.gl/forms/QHHCOPYD8rPIcgbd2"));
        context.startActivity(link);
        agradecerFeedback(context);
    }

    public static void agradecerFeedback(Context context) {
        Intent link = new Intent(Intent.ACTION_VIEW, Uri.parse("https://goo.gl/forms/QHHCOPYD8rPIcgbd2"));
        context.startActivity(link);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.estamos_contentes_feedback);
        builder.setIcon(R.drawable.ic_feedback);
        builder.setMessage(R.string.agradecimento_feedback);
        builder.setCancelable(true);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private static void exibirAgradecimentoAvaliacao(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.estamos_contentes_avaliacao);
        builder.setIcon(R.drawable.ic_star_five);
        builder.setMessage(R.string.agradecimento_avaliacao);
        builder.setCancelable(true);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }
}
