package br.edu.unipampa.apphorarios.adapter;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import br.edu.unipampa.apphorarios.R;
import br.edu.unipampa.apphorarios.controller.MainActivity;
import br.edu.unipampa.apphorarios.fragment.DetalhesHorarioFragment;
import br.edu.unipampa.apphorarios.model.Aula;
import br.edu.unipampa.apphorarios.util.TextViewUtils;


public class AdapterHorario extends RecyclerView.Adapter<AdapterHorario.EditViewHolder> {
    private List<Aula> aulas;
    private Context context;


    public AdapterHorario(List<Aula> aulas, Context context) {
        this.aulas = aulas;
        this.context = context;
    }

    //// INICIO VIEWHOLDER //

    public class EditViewHolder extends RecyclerView.ViewHolder {

        private TextView tvNome;
        private TextView tvSala;
        private TextView tvProfessor;
        private TextView tvTurma;
        private TextView tvHorario;
        private View view;

        private AdapterView.OnItemSelectedListener listener;

        public EditViewHolder(View rowView) {
            super(rowView);
            tvNome = (TextView) rowView.findViewById(R.id.disc);
            tvSala = (TextView) rowView.findViewById(R.id.sala);
            tvProfessor = (TextView) rowView.findViewById(R.id.professor);
            tvTurma = (TextView) rowView.findViewById(R.id.turma);
            tvHorario = (TextView) rowView.findViewById(R.id.hora_sem_conflito);
            this.view = rowView;
        }


    }
    //// FIM VIEWHOLDER //


    @Override
    public EditViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_horario, parent, false);

        return new EditViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final EditViewHolder holder, final int position) {
        if (aulas.size() > position) {
            Aula aula = aulas.get(position);
            holder.tvHorario.setText(aula.getHoraInicio() + " " + aula.getHoraTermino());
            holder.tvNome.setText(aula.getNome());
            holder.tvTurma.setText(aula.getTurma());
            holder.tvProfessor.setText(aula.getProfessor());
            holder.tvSala.setText(aula.getSala());

            TextViewUtils.configurar(holder.tvSala, R.drawable.ic_local);
            TextViewUtils.configurar(holder.tvProfessor, R.drawable.ic_professor);
            TextViewUtils.configurar(holder.tvTurma, R.drawable.ic_turma);

            ImageView ic_notificacao = (ImageView) holder.view.findViewById(R.id.ic_notificacao);
            if (aulas.get(position).getMinutosNotificar() == -1) {
                ic_notificacao.setImageResource(R.drawable.ic_notification_disable);
            } else {
                ic_notificacao.setImageResource(R.drawable.ic_notification);
            }
            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentTransaction ft = ((MainActivity) context).getSupportFragmentManager().beginTransaction();
                    DetalhesHorarioFragment dFragment = new DetalhesHorarioFragment();
                    Bundle args = new Bundle();
                    Aula aula = aulas.get(position);
                    args.putSerializable(context.getString(R.string.aula), aula);
                    dFragment.setArguments(args);
                    dFragment.show(ft, "Dialog Fragment");
                }
            });

            for (Aula aulaCadastrada : aulas) {
                if (aulaCadastrada.hasConflitoHorario(aulas.get(position))) {
                    holder.tvHorario.setBackgroundResource(R.drawable.shape_conflito);
                    holder.tvNome.setTextColor(holder.view.getResources().getColor(R.color.vermelho));
                }
            }


            // ocultar labels vazios
            if (aula.getSala().isEmpty()) {
                holder.tvSala.setVisibility(View.GONE);
            }
            if (aula.getProfessor().isEmpty()) {
                holder.tvProfessor.setVisibility(View.GONE);
            }
            if (aula.getTurma().isEmpty()) {
                holder.tvTurma.setVisibility(View.GONE);
            }

            // remover falsa elevação
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                View cardView = holder.view.findViewById(R.id.cardHorario);
                cardView.setBackgroundColor(holder.view.getResources().getColor(R.color.branco));
            }
        }
    }


    @Override
    public int getItemCount() {
        return aulas.size();
    }


}







