package br.edu.unipampa.apphorarios.controller;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.util.List;

import br.edu.unipampa.apphorarios.R;
import br.edu.unipampa.apphorarios.model.Aula;
import br.edu.unipampa.apphorarios.persistencia.DataBasePersistencia;

public class DecisaoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        // corrigir o tempo de notificação para horários cadastrados anteriormente
        // TO-DO remover isso no próximo semestre
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                corrigeTempoNotificacao();
            }
        });


        decidirMostrarSplash();
    }


    /**
     * Decide se irá mostrar o splash
     */
    private void decidirMostrarSplash() {
        SharedPreferences settings = getSharedPreferences(getString(R.string.prefs), 0);
        boolean isSplash = settings.getBoolean(getString(R.string.isSplash), true);

        Intent i;
        if (isSplash)//
        {
            i = new Intent(DecisaoActivity.this, SplashScreenActivity.class);
        } else {
            i = new Intent(DecisaoActivity.this, MainActivity.class);
        }
        startActivity(i);
        finish();
    }


    /**
     * Esse método irá corrigir o tempo de notificação das aulas que antes usavam 0 e 1 e
     * agora passam a utilizar -1 para sem notificacao, ou o tempo de notificação
     */
    private void corrigeTempoNotificacao() {
        SharedPreferences settings = getSharedPreferences(getString(R.string.prefs), 0);
        boolean tempoCorrigido = settings.getBoolean(getString(R.string.tempoCorrigido), false);
        if (!tempoCorrigido)//
        {
            DataBasePersistencia dao = new DataBasePersistencia(this);
            List<Aula> aulas = dao.select();
            for (Aula aula : aulas) {
                if (aula.getMinutosNotificar() == 1) {
                    aula.setMinutosNotificar(15);
                    dao.update(aula);
                } else if (aula.getMinutosNotificar() == 0) {
                    aula.setMinutosNotificar(-1);
                    dao.update(aula);
                }
            }
            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean(getString(R.string.tempoCorrigido), true);
            editor.apply();
        }
    }

}