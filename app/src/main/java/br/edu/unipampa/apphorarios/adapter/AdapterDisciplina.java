package br.edu.unipampa.apphorarios.adapter;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import java.util.List;

import br.edu.unipampa.apphorarios.R;
import br.edu.unipampa.apphorarios.controller.MainActivity;
import br.edu.unipampa.apphorarios.fragment.DetalhesDisciplinaFragment;
import br.edu.unipampa.apphorarios.model.Aula;
import br.edu.unipampa.apphorarios.model.Disciplina;
import br.edu.unipampa.apphorarios.persistencia.DataBasePersistencia;


public class AdapterDisciplina extends RecyclerView.Adapter<AdapterDisciplina.EditViewHolder> {
    private final Context context;
    private final List<Disciplina> disciplinas;
    private DataBasePersistencia dao;

    public AdapterDisciplina(List<Disciplina> disciplinas, Context context) {
        this.disciplinas = disciplinas;
        this.context = context;
        this.dao = new DataBasePersistencia(context);
        Disciplina.sort(disciplinas);
    }

    //// INICIO VIEWHOLDER //

    public class EditViewHolder extends RecyclerView.ViewHolder {

        private TextView tvNome;
        private TextView tvDias;
        private View view;

        private AdapterView.OnItemSelectedListener listener;

        public EditViewHolder(View rowView) {
            super(rowView);
            tvNome = (TextView) rowView.findViewById(R.id.disc);
            tvDias = (TextView) rowView.findViewById(R.id.dias);
            this.view = rowView;
        }
    }
    //// FIM VIEWHOLDER //


    @Override
    public EditViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_disciplina, parent, false);
        return new EditViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final EditViewHolder holder, final int position) {
        if (disciplinas.size() > position) {
            Disciplina disc = disciplinas.get(position);

            holder.tvNome.setText(disc.getNome());

            String campo = disc.getCurso();
            if (!disc.getSemestre().isEmpty()) {
                campo += " (" + disc.getSemestre() + " semestre) ";
            }

            String dias = "";
            for (Aula aula : disc.getAulas()) {
                String dia = aula.getDiaSemana().replace("-Feira", "");
                if (!dias.contains(dia)) {
                    dias += dia + " - ";
                }
            }
            dias = dias.substring(0, dias.length() - 3);
            holder.tvDias.setText(dias);

            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    android.support.v4.app.FragmentTransaction ft = ((MainActivity) context).getSupportFragmentManager().beginTransaction();
                    DetalhesDisciplinaFragment dFragment = new DetalhesDisciplinaFragment();
                    Bundle args = new Bundle();
                    args.putSerializable(context.getString(R.string.disciplinas), disciplinas.get(position));
                    dFragment.setArguments(args);
                    dFragment.show(ft, "Dialog Fragment");
                }
            });
            // remover falsa elevação
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                View cardView = holder.view.findViewById(R.id.cardDisciplina);
                cardView.setBackgroundColor(holder.view.getResources().getColor(R.color.branco));
            }
        }
    }


    @Override
    public int getItemCount() {
        return disciplinas.size();
    }


}







