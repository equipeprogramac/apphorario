package br.edu.unipampa.apphorarios.controller;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import br.edu.unipampa.apphorarios.R;


public class SplashScreenActivity extends AppCompatActivity implements Runnable {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        String versao = getString(R.string.versao);
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versao += " " + packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            versao += " 2.0";
            e.printStackTrace();
        }


        TextView tvVersao = findViewById(R.id.versao);
        tvVersao.setText(versao);

        Handler handler = new Handler();
        handler.postDelayed(this, 2000); // utilização do handler para deixar o logo aparecendo por 2 segundos
    }


    /**
     * Exibe slash caso esteja ativado
     */
    @Override
    public void run() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
