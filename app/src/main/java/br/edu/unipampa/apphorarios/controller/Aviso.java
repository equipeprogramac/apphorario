package br.edu.unipampa.apphorarios.controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.edu.unipampa.apphorarios.R;
import br.edu.unipampa.apphorarios.model.Aula;
import br.edu.unipampa.apphorarios.persistencia.DataBasePersistencia;

/**
 * Created by marcus on 16/11/2016.
 */

public class Aviso {

    private Activity activity;

    public Aviso(Activity context) {
        this.activity = context;
    }

    /**
     * Método para exibir um alert dialog caso confirmado finalisa a activity, caso contrário se fecha
     *
     * @param message mensagem do alert dialog
     */
    public void showDialogSair(int message) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this.activity);

        builder.setMessage(message).setTitle(null)
                .setCancelable(true)

                .setPositiveButton(R.string.sim, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        activity.finish();

                    }
                })
                .setNeutralButton(R.string.cancelar, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }


    public void showDialogInformacao(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.activity);

        builder.setMessage(message).setTitle(null)
                .setCancelable(true)
                .setPositiveButton(activity.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    /*
     *   Método para exibir um toast contendo uma mensagem
     *   @param message mensagem do toast
     */
    public void toastMessager(int mensagem) {
        SingleToast.show(activity, activity.getString(mensagem), Toast.LENGTH_LONG);
    }

    public void showDialog(String message, int positiveLabel, DialogInterface.OnClickListener positiveAction, int negativeLabel) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.activity);
        builder.setMessage(message).setTitle(null)
                .setCancelable(true)
                .setPositiveButton(positiveLabel, positiveAction)
                .setNegativeButton(negativeLabel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }


    /**
     * Classe para previnir que mais de um toast seja iniciado ao mesmo tempo
     */
    public static class SingleToast {

        private static Toast mToast;

        public static void show(Context context, String text, int duration) {
            if (mToast != null) mToast.cancel();
            mToast = Toast.makeText(context, text, duration);
            mToast.show();
        }
    }

    /**
     * Método para exibir um alert dialog com opções de deletar apenas uma aula ou todas as aulas da disciplina
     *
     * @param activityExclusao
     * @param message
     * @param aula
     * @param detalhesFragment
     */
    public void showDialogExclusao(final Activity activityExclusao, String message, final Aula aula, final Dialog detalhesFragment) {
        final DataBasePersistencia dao = new DataBasePersistencia(activityExclusao);
        final String[] items;
        if (aula.getDisciplina().getAulas().size() == 1) {
            items = new String[]{activityExclusao.getString(R.string.excluir_um)};
        } else {
            items = new String[]{activityExclusao.getString(R.string.excluir_um), activityExclusao.getString(R.string.excluir_todos)};
        }
        //  final String[] items = {activityExclusao.getString(R.string.excluir_um), activityExclusao.getString(R.string.excluir_todos)};
        final List<Integer> checkedItems = new ArrayList<>();
        final NotificacaoController notificacaoController = new NotificacaoController(activityExclusao);

        final AlertDialog.Builder builder = new AlertDialog.Builder(activityExclusao);


        builder.setTitle(message)
                .setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        checkedItems.clear();
                        checkedItems.add(item);
                    }

                })
                //    .setIcon(R.drawable.ic_remove)

                .setCancelable(true)

                .setPositiveButton(activityExclusao.getText(R.string.excluir), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (checkedItems.isEmpty()) {
                            //   checkedItems.clear();
                            checkedItems.add(0);
                        }
                        if (checkedItems.get(0).equals(0)) {
                            dao.delete(aula);
                            notificacaoController.cancelarNotificacao(aula);
                            Toast.makeText(activityExclusao.getBaseContext(), activityExclusao.getString(R.string.exclusao_aula_sucesso),
                                    Toast.LENGTH_LONG).show();
                            detalhesFragment.dismiss();
                            activityExclusao.recreate();
                        } else {
                            for (Aula aulaDelete : aula.getDisciplina().getAulas()) {
                                Integer idHorario = (int) dao.delete(aulaDelete);
                                notificacaoController.cancelarNotificacao(aulaDelete);
                            }
                            Toast.makeText(activityExclusao.getBaseContext(), activityExclusao.getString(R.string.exclusao_aulas_sucesso),
                                    Toast.LENGTH_LONG).show();
                            detalhesFragment.dismiss();
                            activityExclusao.recreate();
                        }

                    }
                })
                .setNeutralButton(activityExclusao.getString(R.string.cancelar), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();

        alert.show();

    }

    public void snackBar(int mensagem) {
        Snackbar s = Snackbar.make(activity.findViewById(android.R.id.content), mensagem, Snackbar.LENGTH_LONG);
        s.show();
    }

    public void snackBar(int mensagem, int label, View.OnClickListener action) {
        Snackbar s = Snackbar.make(activity.findViewById(android.R.id.content), mensagem, Snackbar.LENGTH_LONG);
        s.setAction(label, action);
        View snackbarView = s.getView();
        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setMaxLines(6);
        s.show();
    }

    /**
     * Método para exibir um alert dialog caso confirmado finalisa a activity, caso contrário se fecha
     *
     * @param message mensagem do alert dialog
     */
    public void showDialog(String message, int positiveLabel, DialogInterface.OnClickListener positiveAction) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.activity);
        builder.setMessage(message).setTitle(null)
                .setCancelable(true)
                .setPositiveButton(positiveLabel, positiveAction)
                .setNeutralButton(R.string.cancelar, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    /**
     * Método para exibir um alert dialog caso confirmado finalisa a activity, caso contrário se fecha
     *
     * @param message mensagem do alert dialog
     */
    public AlertDialog.Builder buildDialog(String message, int positiveLabel, DialogInterface.OnClickListener positiveAction) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.activity);
        builder.setMessage(message).setTitle(null)
                .setCancelable(true)
                .setPositiveButton(positiveLabel, positiveAction)
                .setNeutralButton(R.string.cancelar, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                    }
                });
        return builder;
    }


    public void showDialog(int titulo, int icone, String message, int positiveLabel, DialogInterface.OnClickListener positiveAction) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.activity);
        builder.setMessage(message).setTitle(titulo).setIcon(icone)
                .setCancelable(true)
                .setPositiveButton(positiveLabel, positiveAction)
                .setNeutralButton(R.string.cancelar, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

}
