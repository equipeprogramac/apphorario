package br.edu.unipampa.apphorarios.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;

import java.util.concurrent.TimeUnit;

import br.edu.unipampa.apphorarios.controller.NotificacaoController;
import br.edu.unipampa.apphorarios.model.Aula;
import br.edu.unipampa.apphorarios.util.TempoUtils;

public class AlarmReceiver extends BroadcastReceiver {

    public static String NOTIFICATION_ID = "notification_id";

    @Override
    public void onReceive(final Context context, Intent intent) {
        if (!isBoot(context, intent)) { // se não é boot é porque está na hora de despertar
            NotificacaoController controller = new NotificacaoController(context);
            int id = intent.getIntExtra(NOTIFICATION_ID, -1);
            if (id > -1) {
                controller.showNotificacao(id);
            } else {
                controller.showNotificacao(controller.getProximaAulaNotificacao());
            }

        }
    }

    /**
     * Verifica se foi realizado o boot no sistema, caso sim irá agendar as notificações de todas as aulas
     *
     * @param context
     * @param intent
     * @return
     */
    public boolean isBoot(Context context, Intent intent) {
        NotificacaoController controller = new NotificacaoController(context);
        if (intent.getAction() != null) {
            if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")
                    || intent.getAction().equals("android.intent.action.REBOOT")
                    || intent.getAction().equals("android.intent.action.QUICKBOOT_POWERON")) {
                controller.prepararNotificacoes(); // deixa todas as notificações agendadas
                return true;
            }
        }
        return false;
    }

    /**
     * Agenda o horário do alarme no horário previsto para notificar a aula
     *
     * @param aula
     * @param context
     */
    public void agendarAlarme(@NonNull Aula aula, Context context) {
        // Enable a receiver
        ComponentName receiver = new ComponentName(context, AlarmReceiver.class);
        PackageManager pm = context.getPackageManager();
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);

        int notificationId = aula.getId();/////

        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtra(AlarmReceiver.NOTIFICATION_ID, notificationId);

        AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        PendingIntent alarmIntent;
        if (PendingIntent.getBroadcast(context, notificationId, intent, PendingIntent.FLAG_NO_CREATE) == null) {
            alarmIntent = PendingIntent.getBroadcast(context, notificationId, intent, 0);
        } else {
            alarmIntent = PendingIntent.getBroadcast(context, notificationId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        }

        long tempo = TempoUtils.millisTempoNotificacao(aula);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmMgr.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, tempo, alarmIntent);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            alarmMgr.setExact(AlarmManager.RTC_WAKEUP, tempo, alarmIntent);
        } else {
            alarmMgr.set(AlarmManager.RTC_WAKEUP, tempo, alarmIntent);
        }
        alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP, tempo, TimeUnit.DAYS.toMillis(7), alarmIntent); // repetir a cada 7 dias
    }
}