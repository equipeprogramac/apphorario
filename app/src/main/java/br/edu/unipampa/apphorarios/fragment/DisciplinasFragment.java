package br.edu.unipampa.apphorarios.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.edu.unipampa.apphorarios.R;
import br.edu.unipampa.apphorarios.adapter.AdapterDisciplina;
import br.edu.unipampa.apphorarios.controller.AdicaoActivity;
import br.edu.unipampa.apphorarios.model.Aula;
import br.edu.unipampa.apphorarios.model.Disciplina;
import br.edu.unipampa.apphorarios.persistencia.DataBasePersistencia;

public class DisciplinasFragment extends Fragment implements View.OnClickListener {


    private View rootView;
    private RecyclerView recyclerView;

    @Override
    public void onStart() {
        super.onStart();
        atualizarList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_disciplinas, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.listDisciplinas);
        recyclerView.setOnClickListener(this);
        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.floatingAddButton);
        fab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(rootView.getContext(), AdicaoActivity.class);
                startActivity(intent);
            }
        });
        return rootView;
    }


    /*
     * Método para popular as listas de cada dia da semana com as aulas do banco de dados.
     */
    private void atualizarList() {
        DataBasePersistencia dao = new DataBasePersistencia(getContext());
        List<Disciplina> disciplinas = dao.selectDisciplinas();

        AdapterDisciplina adapter = new AdapterDisciplina(disciplinas, rootView.getContext());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);

        TextView emptyView = (TextView) rootView.findViewById(R.id.empty_view);
        if (disciplinas.isEmpty()) {
            emptyView.setVisibility(View.VISIBLE);
        } else {
            emptyView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(getActivity(), DetalhesHorarioFragment.class);
        startActivity(intent);

    }
}
