package br.edu.unipampa.apphorarios.persistencia;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class AulaDBHelper extends SQLiteOpenHelper {

    private static final String BANCO_NOME = "aulas.db";
    private static final int BANCO_VERSAO = 1;

    public AulaDBHelper(Context context) {
        super(context, BANCO_NOME, null, BANCO_VERSAO);
    }


    private static final String SQL_CREATE_TABLE_HORAIO =
            "CREATE TABLE " + DataBaseContract.Horario.NOME_TABELA + " (" +
                    DataBaseContract.Horario._ID + " INTEGER PRIMARY KEY," +
                    DataBaseContract.Horario.COLUNA_DIA+ " TEXT, "+
                    DataBaseContract.Horario.COLUNA_HORARIO_INICIO + " TEXT, " +
                    DataBaseContract.Horario.COLUNA_HORARIO_FINAL + " TEXT);";


    private static final String SQL_CREATE_TABLE_TURMA=
            "CREATE TABLE " + DataBaseContract.Turma.NOME_TABELA + " (" +
                    DataBaseContract.Turma._ID + " INTEGER PRIMARY KEY," +
                    DataBaseContract.Turma.COLUNA_TURMA + " TEXT);";

    private static final String SQL_CREATE_TABLE_CURSO=
            "CREATE TABLE " + DataBaseContract.Curso.NOME_TABELA + " (" +
                    DataBaseContract.Curso._ID + " INTEGER PRIMARY KEY," +
                    DataBaseContract.Curso.COLUNA_CURSO + " TEXT);";

    private static final String SQL_CREATE_TABLE_PROFESSOR=
            "CREATE TABLE " + DataBaseContract.Professor.NOME_TABELA + " (" +
                    DataBaseContract.Professor._ID + " INTEGER PRIMARY KEY," +
                    DataBaseContract.Professor.COLUNA_PROFESSOR + " TEXT);";

    private static final String SQL_CREATE_TABLE_AULA=
            "CREATE TABLE " + DataBaseContract.Aula.NOME_TABELA + " (" +
                    DataBaseContract.Aula._ID + " INTEGER PRIMARY KEY," +
                    DataBaseContract.Aula.COLUNA_FK_HORARIO+" int not null," +
                    DataBaseContract.Aula.COLUNA_SALA+" TEXT," +
                    "FOREIGN KEY ("+ DataBaseContract.Aula.COLUNA_FK_HORARIO+") REFERENCES "+ DataBaseContract.Horario.NOME_TABELA+"("+DataBaseContract.Horario._ID+"));";

    private static final String SQL_CREATE_TABLE_DISCIPLINA=
            "CREATE TABLE " + DataBaseContract.Disciplina.NOME_TABELA + " (" +
                    DataBaseContract.Disciplina._ID + " INTEGER PRIMARY KEY," +
                    DataBaseContract.Disciplina.COLUNA_NOME_DISCIPLINA + " TEXT , " +
                    DataBaseContract.Disciplina.COLUNA_SEMESTRE +" TEXT,"+
                    DataBaseContract.Disciplina.COLUNA_NOTIFICACAO +" int,"+
                    DataBaseContract.Disciplina.COLUNA_FK_CURSO +" int not null," +
                    DataBaseContract.Disciplina.COLUNA_FK_PROFESSOR+" int not null," +
                    DataBaseContract.Disciplina.COLUNA_FK_TURMA+" int not null, " +
                    "FOREIGN KEY ("+ DataBaseContract.Disciplina.COLUNA_FK_CURSO+") REFERENCES "+ DataBaseContract.Curso.NOME_TABELA+"("+DataBaseContract.Curso._ID+"), " +
                    "FOREIGN KEY ("+ DataBaseContract.Disciplina.COLUNA_FK_PROFESSOR+") REFERENCES "+ DataBaseContract.Professor.NOME_TABELA+"("+DataBaseContract.Professor._ID+"), " +
                    "FOREIGN KEY ("+ DataBaseContract.Disciplina.COLUNA_FK_TURMA+") REFERENCES "+ DataBaseContract.Turma.NOME_TABELA+"("+DataBaseContract.Turma._ID+"));";

    private static final String SQL_CREATE_TABLE_AULA_HAS_DISCIPLINA=
            "CREATE TABLE " + DataBaseContract.Aula_has_disciplinas.NOME_TABELA + " (" +
                    DataBaseContract.Aula_has_disciplinas._ID + " INTEGER PRIMARY KEY," +
                    DataBaseContract.Aula_has_disciplinas.FK_AULA +" TEXT,"+
                    DataBaseContract.Aula_has_disciplinas.FK_DISCIPLINA +" TEXT,"+
                    "FOREIGN KEY ("+ DataBaseContract.Aula_has_disciplinas.FK_AULA+") REFERENCES "+ DataBaseContract.Aula.NOME_TABELA+" ("+DataBaseContract.Aula._ID+")," +
                    "FOREIGN KEY ("+ DataBaseContract.Aula_has_disciplinas.FK_DISCIPLINA+") REFERENCES "+ DataBaseContract.Disciplina.NOME_TABELA+" ("+DataBaseContract.Disciplina._ID+"));";

    private static final String SQL_CREATE_TABLE_ALUNO=
            "CREATE TABLE " + DataBaseContract.Aluno.NOME_TABELA + " (" +
                    DataBaseContract.Aluno.COLUNA_FK_AULA_HAS_DISCIPLINA+" int not null, "+
                    "FOREIGN KEY ("+ DataBaseContract.Aluno.COLUNA_FK_AULA_HAS_DISCIPLINA+") REFERENCES "+ DataBaseContract.Aula_has_disciplinas.NOME_TABELA+" ("+DataBaseContract.Aula_has_disciplinas._ID+"));";

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE_HORAIO);
        db.execSQL(SQL_CREATE_TABLE_TURMA);
        db.execSQL(SQL_CREATE_TABLE_CURSO);
        db.execSQL(SQL_CREATE_TABLE_PROFESSOR);
        db.execSQL(SQL_CREATE_TABLE_AULA);
        db.execSQL(SQL_CREATE_TABLE_DISCIPLINA);
        db.execSQL(SQL_CREATE_TABLE_AULA_HAS_DISCIPLINA);
        db.execSQL(SQL_CREATE_TABLE_ALUNO);
        Log.d("Banco de Dados","Criando banco de dados.");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        /*
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
        */
    }
}