package br.edu.unipampa.apphorarios.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import br.edu.unipampa.apphorarios.R;
import br.edu.unipampa.apphorarios.controller.Aviso;
import br.edu.unipampa.apphorarios.controller.EdicaoActivity;
import br.edu.unipampa.apphorarios.controller.MainActivity;
import br.edu.unipampa.apphorarios.model.Aula;
import br.edu.unipampa.apphorarios.util.TextViewUtils;
import br.edu.unipampa.apphorarios.util.TempoUtils;


public class DetalhesHorarioFragment extends DialogFragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.DialogAnimation;
        getDialog().setCanceledOnTouchOutside(true);

        View rootView = inflater.inflate(R.layout.fragment_detalhes_aula, container, false);
        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.my_toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow_white);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        toolbar.inflateMenu(R.menu.menu_dialog_fragment);

        Bundle bundle = getArguments();
        final Aula aula = (Aula) bundle.getSerializable(getString(R.string.aula));

        exibeInformacoesAula(aula, rootView);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_editar:
                        Intent intent = new Intent(getActivity(), EdicaoActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(getString(R.string.disciplina), aula.getDisciplina());
                        intent.putExtra(getString(R.string.disciplina), aula.getDisciplina());
                        getActivity().startActivity(intent);
                        getDialog().dismiss();
                        return true;

                    case R.id.action_excluir:
                        mostrarConfirmacaoExclusao(aula);
                        return true;
                    default:
                        return true;

                }
            }
        });
        return rootView;
    }

    private void mostrarConfirmacaoExclusao(Aula aula) {
        Aviso aviso = new Aviso(getActivity());
        aviso.showDialogExclusao(getActivity(), getString(R.string.pergunta_exclusao), aula, getDialog());
        final String[] items;
        if (aula.getDisciplina().getAulas().size() == 1) {
            items = new String[]{getString(R.string.excluir_um)};
        } else {
            items = new String[]{getString(R.string.excluir_um), getString(R.string.excluir_todos)};
        }

    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }

    /**
     * Exibir informações da aula
     *
     * @param aula
     * @param rootView
     */
    private void exibeInformacoesAula(Aula aula, View rootView) {

        TextView nome = (TextView) rootView.findViewById(R.id.nome);
        TextView tvProfessor = (TextView) rootView.findViewById(R.id.professor);
        TextView tvHorario = (TextView) rootView.findViewById(R.id.horario);
        TextView tvTurma = (TextView) rootView.findViewById(R.id.turma);
        TextView tvSala = (TextView) rootView.findViewById(R.id.sala);
        TextView tvCurso = (TextView) rootView.findViewById(R.id.curso);
        TextView tvSemestre = (TextView) rootView.findViewById(R.id.semestre);
        TextView tvDiaSemana = (TextView) rootView.findViewById(R.id.diaSemana);
        TextView tvHorarioTermino = (TextView) rootView.findViewById(R.id.horario_termino);

        tvProfessor.setText(getString(R.string.professor) + ": " + aula.getProfessor());
        tvHorario.setText(getString(R.string.horario) + ": " + aula.getHoraInicio() + " às " + aula.getHoraTermino());
        tvHorarioTermino.setText(getString(R.string.horarioTermino) + ": " + aula.getHoraTermino());
        tvTurma.setText(getString(R.string.turma) + ": " + aula.getTurma());
        tvSala.setText(getString(R.string.sala) + ": " + aula.getSala());
        tvCurso.setText(getString(R.string.curso) + ": " + aula.getCurso());
        tvSemestre.setText(getString(R.string.semestre) + ": " + aula.getSemestre());
        tvDiaSemana.setText(getString(R.string.dia) + ": " + aula.getDiaSemana());
        nome.setText(aula.getNome());

        // exibe informações sobre a notificação
        TextView tvNotificacao = (TextView) rootView.findViewById(R.id.notificacao);
        if (aula.getMinutosNotificar() > -1) {
            String texto = getString(R.string.notificacao) + ":";
            texto += TempoUtils.formatTempoNotificacao((int) aula.getMinutosNotificar());
            tvNotificacao.setText(texto);
        } else {
            tvNotificacao = (TextView) rootView.findViewById(R.id.sem_notificacao);
            tvNotificacao.setText(getString(R.string.notificacao) + ": Não");
        }

        tvNotificacao.setVisibility(View.VISIBLE);


        // exibe apenas os campos preenchidos
        if (aula.getTurma().isEmpty()) {
            tvTurma.setVisibility(View.GONE);
        }
        if (aula.getSala().isEmpty()) {
            tvSala.setVisibility(View.GONE);
        }
        if (aula.getProfessor().isEmpty()) {
            tvProfessor.setVisibility(View.GONE);
        }
        if (aula.getCurso().isEmpty() || !((MainActivity) getActivity()).isUniversitario()) {
            tvCurso.setVisibility(View.GONE);
        }
        if (aula.getSemestre().isEmpty() || !((MainActivity) getActivity()).isUniversitario()) {
            tvSemestre.setVisibility(View.GONE);
        }


        // deixando os ícones mais suaves
        TextViewUtils.changeIconColor(tvProfessor, R.color.icone);
        TextViewUtils.changeIconColor(tvHorario, R.color.icone);
        TextViewUtils.changeIconColor(tvHorarioTermino, R.color.icone);
        TextViewUtils.changeIconColor(tvTurma, R.color.icone);
        TextViewUtils.changeIconColor(tvSala, R.color.icone);
        TextViewUtils.changeIconColor(tvCurso, R.color.icone);
        TextViewUtils.changeIconColor(tvDiaSemana, R.color.icone);
        TextViewUtils.changeIconColor(tvSemestre, R.color.icone);
        TextViewUtils.changeIconColor(tvNotificacao, R.color.icone);

        TextViewUtils.editarEstilo(tvProfessor);
        TextViewUtils.editarEstilo(tvHorario);
        TextViewUtils.editarEstilo(tvHorarioTermino);
        TextViewUtils.editarEstilo(tvTurma);
        TextViewUtils.editarEstilo(tvSala);
        TextViewUtils.editarEstilo(tvCurso);
        TextViewUtils.editarEstilo(tvDiaSemana);
        TextViewUtils.editarEstilo(tvSemestre);
        TextViewUtils.editarEstilo(tvNotificacao);
    }


}
