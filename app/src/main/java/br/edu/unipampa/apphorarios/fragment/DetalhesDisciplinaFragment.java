package br.edu.unipampa.apphorarios.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import br.edu.unipampa.apphorarios.R;
import br.edu.unipampa.apphorarios.controller.Aviso;
import br.edu.unipampa.apphorarios.controller.EdicaoActivity;
import br.edu.unipampa.apphorarios.controller.MainActivity;
import br.edu.unipampa.apphorarios.model.Aula;
import br.edu.unipampa.apphorarios.model.Disciplina;
import br.edu.unipampa.apphorarios.persistencia.DataBasePersistencia;
import br.edu.unipampa.apphorarios.util.TextViewUtils;
import br.edu.unipampa.apphorarios.util.TempoUtils;


public class DetalhesDisciplinaFragment extends DialogFragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.DialogAnimation;
        getDialog().setCanceledOnTouchOutside(true);


        //remover divisor azul do dialog
        View rootView = inflater.inflate(R.layout.fragment_detalhes_disciplina, container, false);
        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.my_toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow_white);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        toolbar.inflateMenu(R.menu.menu_dialog_fragment);

        Bundle bundle = getArguments();
        final Disciplina disciplina = (Disciplina) bundle.getSerializable(getString(R.string.disciplinas));

        exibeInformacoes(disciplina, rootView);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_editar:
                        Intent intent = new Intent(getActivity(), EdicaoActivity.class);
                        intent.putExtra(getString(R.string.disciplina), disciplina);
                        getActivity().startActivity(intent);
                        getDialog().dismiss();
                        return true;

                    case R.id.action_excluir:
                        Aviso aviso = new Aviso(getActivity());
                        aviso.showDialog(getString(R.string.aviso_exclusao_disciplina), R.string.excluir,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        DataBasePersistencia dao = new DataBasePersistencia(getContext());
                                        for (Aula aula : disciplina.getAulas()) {
                                            dao.delete(aula);
                                        }
                                        Intent intent = new Intent(getActivity(), MainActivity.class);
                                        intent.putExtra(getString(R.string.disciplina), true);
                                        getDialog().dismiss();
                                        getActivity().finish();
                                        startActivity(intent);
                                    }
                                });
                        return true;
                    default:
                        return true;

                }
            }
        });
        return rootView;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    /**
     * Exibir informações da aula
     *
     * @param disciplina
     * @param rootView
     */
    private void exibeInformacoes(Disciplina disciplina, View rootView) {

        TextView nome = (TextView) rootView.findViewById(R.id.nome);
        TextView tvProfessor = (TextView) rootView.findViewById(R.id.professor);
        TextView tvTurma = (TextView) rootView.findViewById(R.id.turma);
        TextView tvCurso = (TextView) rootView.findViewById(R.id.curso);
        TextView tvSemestre = (TextView) rootView.findViewById(R.id.semestre);

        tvProfessor.setText(getString(R.string.professor) + ": " + disciplina.getProfessor());
        tvTurma.setText(getString(R.string.turma) + ": " + disciplina.getTurma());
        tvCurso.setText(getString(R.string.curso) + ": " + disciplina.getCurso());
        tvSemestre.setText(getString(R.string.semestre) + ": " + disciplina.getSemestre());
        nome.setText(disciplina.getNome());

        // exibe informações sobre a notificação
        TextView tvNotificacao = (TextView) rootView.findViewById(R.id.notificacao);
        if (disciplina.getMinutosNotificar() > -1) {
            String texto = getString(R.string.notificacao) + ":";
            texto += TempoUtils.formatTempoNotificacao(disciplina.getMinutosNotificar());
            tvNotificacao.setText(texto);
        } else {
            tvNotificacao = (TextView) rootView.findViewById(R.id.sem_notificacao);
            tvNotificacao.setText(getString(R.string.notificacao) + ": Não");
        }

        tvNotificacao.setVisibility(View.VISIBLE);

        LinearLayout linearLayout = rootView.findViewById(R.id.linearDisciplina);
        for (Aula aula : disciplina.getAulas()) {
            TextView tvNova = new TextView(getContext());
            String exibicao = aula.getDiaSemana() + ": " + aula.getHoraInicio() + " às " + aula.getHoraTermino();
            if (!aula.getSala().isEmpty()) {
                exibicao += " / " + aula.getSala();
            }
            tvNova.setText(exibicao);
            tvNova.setTextColor(getContext().getResources().getColor(R.color.preto));
            tvNova.setTextSize(TypedValue.COMPLEX_UNIT_PT, 9);
            TextViewUtils.editarEstilo(tvNova);
            linearLayout.addView(tvNova);
        }


        // exibe apenas os campos preenchidos
        if (disciplina.getTurma().isEmpty()) {
            tvTurma.setVisibility(View.GONE);
        }
        if (disciplina.getProfessor().isEmpty()) {
            tvProfessor.setVisibility(View.GONE);
        }
        if (disciplina.getCurso().isEmpty() || !((MainActivity) getActivity()).isUniversitario()) {
            tvCurso.setVisibility(View.GONE);
        }
        if (disciplina.getSemestre().isEmpty() || !((MainActivity) getActivity()).isUniversitario()) {
            tvSemestre.setVisibility(View.GONE);
        }

        // deixando os ícones mais suaves
        TextViewUtils.changeIconColor(tvProfessor, R.color.icone);
        TextViewUtils.changeIconColor(tvTurma, R.color.icone);
        TextViewUtils.changeIconColor(tvCurso, R.color.icone);
        TextViewUtils.changeIconColor(tvSemestre, R.color.icone);
        TextViewUtils.changeIconColor(tvNotificacao, R.color.icone);


        TextViewUtils.editarEstilo(tvProfessor);
        TextViewUtils.editarEstilo(tvTurma);
        TextViewUtils.editarEstilo(tvCurso);
        TextViewUtils.editarEstilo(tvSemestre);
        TextViewUtils.editarEstilo(tvNotificacao);
    }


}
